# Personal Website of a Musician

In my 2nd semester, I worked on a team project where we had to create a personal website for an artist called Miss Starling. 
I was a researcher, UX designer and I was also included in the website's coding. 

Our project was named "The best project" in our class and by our overall performance, we absolutely satisfied all expectations of our client and our teachers.
